# README #

This application is a research tool developed for my master thesis "Random graphs and their applications to fMRI data analysis" (2014-2015), Dubna University, Russia.

In this work it was shown that functional brain networks constructed from fMRI data (in rest) can be approximated well enough by the networks built from the time series of an Ising-like dynamic model. The analysis was based on comparison of topological properties of these networks.

### Research & implementation details ###

The app is written in C# using .NET 4.5 features.

* It allows to generate the time series of 'activity' values for every node (item) of some dynamic network-like structure. There are various generation options, all of them are based on the Ising model but some include certain modifications: standard square lattice or more sophisticated small-world networks. The generation is done using the Metropolis (Monte-Carlo) algorithm.

* For resulting time series we construct their pair wise correlations matrix then given by some threshold value we build a graph. Then we calculate some topological properties of this graph using efficient multithreaded algorithms. The app exports this final data to .csv file.

* The topological properties of the resulting graph are then compared to fMRI data networks properties which are built using the same correlations matrix method.


### Abstract ###
An investigation into mutual action of different brain regions has been very actual and complex scientific challenge. One of the modern study techniques is fMRI (functional magnetic resonance imaging) which measures patient’s brain activity by detecting associated changes in blood flow. The analysis is sometimes performed in terms of complex networks theory which characterizes and distinguishes real world systems based on their topological properties.
Recent studies presented a way to describe the dynamics of the functional brain networks by the Ising model. However, today it is known that the brain is a small-world structure whereas the Ising model is based on a regular lattice which does not share this property.
In this study we propose a generalization of the Ising model for modeling the dynamics of the functional brain networks with respect to its small-world topology. The following numeric simulations were done according to the proposed model: time series generation, correlation matrix and network construction and comparison of the resulting topological properties. It was shown that the proposed model near the critical point nicely represents the dynamics of the functional brain networks.