﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ising
{
    public class IsingSmallWorldAddLinksPreferably : IsingRegularLattice
    {
        // preferential attachment O(N)
        int deg_sum;

        public IsingSmallWorldAddLinksPreferably(Model model, Form1 callbacks)
            : base(model, callbacks)
        { }

        public override void GenerateTopology(CancellationToken cancelToken)
        {
            callbacks.Invoke(callbacks.actionChangeStatus, "Constructing topology...");

            base.GenerateTopology(cancelToken);

            int nodes = MathUtils.Sqr(model.n);
            double addProb = (model as ModelSmallWorldAddLinksPreferably).addProb;

            callbacks.Invoke(callbacks.actionInitProgress, nodes);

            // init preferential attachment
            deg_sum = 0;
            for (int i = 0; i < nodes; i++)
                deg_sum += topology[i].Count;

            // add new links with preferential attachment
            for (int i = 0; i < nodes; i++)
            {
                if (!MathUtils.RandomTry(addProb))
                    continue;

                if (topology[i].Count == nodes - 1)
                    continue;

                int nodeAddLink;
                do
                    nodeAddLink = ChooseNodePreferably();
                while (nodeAddLink == i || topology[i].Contains(nodeAddLink));

                topology[i].Add(nodeAddLink);
                topology[nodeAddLink].Add(i);

                deg_sum += 2;

                cancelToken.ThrowIfCancellationRequested();
                callbacks.Invoke(callbacks.actionIncProgress);
            }

            CheckTopology(addProb, cancelToken);
        }

        public int ChooseNodePreferably()
        {
            int nodes = topology.Length;
            int rnd = StaticRandom.Next(deg_sum);
            int sum = 0;
            int curr;
            for (curr = 0; curr < nodes; curr++)
            {
                if (sum + topology[curr].Count > rnd)
                    break;

                sum += topology[curr].Count;
            }
            return curr;
        }
    }
}
