﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ising
{
    public partial class Form1 : Form
    {
        enum Mode
        {
            None,
            Generate,
            Process
        }

        Mode mode;
        Model model;
        Correlations corr;

        bool generatedOnce;

        int run;
        string mainDir, dir;
        bool dirCreated;

        IsingCalculation calc;
        IsingVisualization vis;

        Task task;
        CancellationTokenSource cancelTokenSource;

        public Action<int> actionInitProgress;
        public Action actionIncProgress;
        public Action<string> actionChangeStatus;
        public Action actionUpdateButton;
        public Action actionVisInit;
        public Action actionVisDraw;
        public Action<int, int> actionVisDrawCell;

        List<ModelType> modelOrder;

        public bool onlyQueryPath = false;
        Dictionary<double, double> probToAveragePath;


        public Form1()
        {
            InitializeComponent();

            actionChangeStatus = status => label5.Text = status;

            actionUpdateButton = () =>
            {
                if (mode == Mode.None)
                {
                    button1.Enabled = button2.Enabled = true;
                    button3.Enabled = false;

                    checkBox1.Enabled = generatedOnce;
                }
                else
                {
                    button1.Enabled = button2.Enabled = false;
                    button3.Enabled = true;

                    checkBox1.Enabled = false;
                }
            };

            actionInitProgress = maxValue =>
            {
                progressBar1.Maximum = maxValue;
                progressBar1.Value = 0;
            };
            actionIncProgress = () => progressBar1.Value++;

            actionVisInit = () =>
            {
                if (vis != null)
                {
                    vis.Dispose();
                    vis = null;
                }

                vis = new IsingVisualization();
                vis.Show();

                if (model != null)
                    vis.Init(model.n);
            };
            actionVisDraw = () =>
            {
                if (!vis.IsDisposed && calc != null)
                    vis.Draw(calc);
            };
            actionVisDrawCell = (i, j) =>
            {
                if (!vis.IsDisposed && calc != null)
                    vis.Draw(calc, i * calc.model.n + j);
            };

            StaticRandom.Init();
            mode = Mode.None;
            generatedOnce = false;
            dirCreated = false;
            mainDir = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            string subdir = DateTime.Now.ToString().Replace(':', '.');
            dir = mainDir + "\\" + subdir;

            modelOrder = new List<ModelType> 
            { 
                ModelType.RegularLattice,
                ModelType.SmallWorldRewiringRandomly,
                ModelType.SmallWorldRewiringPreferably,
                ModelType.SmallWorldAddLinksRandomly,
                ModelType.SmallWorldAddLinksPreferably,

                ModelType.ErdosRenyiRewiring,
            };
            foreach (var item in modelOrder)
                comboBoxTopology.Items.Add(Model.ModelDescription[item]);
            comboBoxTopology.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitState();
        }

        private void InitState()
        {
            Invoke(actionUpdateButton);
            if (mode == Mode.None)
            {
                Invoke(actionChangeStatus, "Ready");
                Invoke(actionInitProgress, 0);
            }
        }


        private void comboBoxTopology_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (modelOrder[comboBoxTopology.SelectedIndex])
            {
                case ModelType.SmallWorldRewiringRandomly:
                case ModelType.SmallWorldRewiringPreferably:

                case ModelType.ErdosRenyiRewiring:
                    labelTopology.Visible = numericUpDownTopology.Visible = true;
                    labelTopology.Text = "Rewire probability";
                    break;

                case ModelType.SmallWorldAddLinksRandomly:
                case ModelType.SmallWorldAddLinksPreferably:
                    labelTopology.Visible = numericUpDownTopology.Visible = true;
                    labelTopology.Text = "New link probability";
                    break;

                case ModelType.RegularLattice:
                default:
                    labelTopology.Visible = numericUpDownTopology.Visible = false;
                    break;
            }
        }

        private void CreateModel(double prob)
        {
            switch (modelOrder[comboBoxTopology.SelectedIndex])
            {
                case ModelType.RegularLattice:
                    model = new ModelRegularLattice();
                    break;
                case ModelType.SmallWorldRewiringRandomly:
                    model = new ModelSmallWorldRewiringRandomly();
                    ((ModelSmallWorldRewiringRandomly)model).rewireProb = prob;
                    break;
                case ModelType.SmallWorldRewiringPreferably:
                    model = new ModelSmallWorldRewiringPreferably();
                    ((ModelSmallWorldRewiringPreferably)model).rewireProb = prob;
                    break;
                case ModelType.SmallWorldAddLinksRandomly:
                    model = new ModelSmallWorldAddLinksRandomly();
                    ((ModelSmallWorldAddLinksRandomly)model).addProb = prob;
                    break;
                case ModelType.SmallWorldAddLinksPreferably:
                    model = new ModelSmallWorldAddLinksPreferably();
                    ((ModelSmallWorldAddLinksPreferably)model).addProb = prob;
                    break;

                case ModelType.ErdosRenyiRewiring:
                    model = new ModelErdosRenyiRewiring();
                    ((ModelErdosRenyiRewiring)model).rewireProb = prob;
                    break;

                default:
                    model = null;
                    return;
            }

            model.n = (int)numericUpDown1.Value;
            model.kT = (double)numericUpDown2.Value;
            model.iter = (int)numericUpDown3.Value;
        }
        private void CreateModel()
        {
            CreateModel((double)numericUpDownTopology.Value);
        }

        private Model CreateModel(Model m, double prob)
        {
            Model model;
            switch (m.modelType)
            {
                case ModelType.RegularLattice:
                    model = new ModelRegularLattice();
                    break;
                case ModelType.SmallWorldRewiringRandomly:
                    model = new ModelSmallWorldRewiringRandomly();
                    ((ModelSmallWorldRewiringRandomly)model).rewireProb = prob;
                    break;
                case ModelType.SmallWorldRewiringPreferably:
                    model = new ModelSmallWorldRewiringPreferably();
                    ((ModelSmallWorldRewiringPreferably)model).rewireProb = prob;
                    break;
                case ModelType.SmallWorldAddLinksRandomly:
                    model = new ModelSmallWorldAddLinksRandomly();
                    ((ModelSmallWorldAddLinksRandomly)model).addProb = prob;
                    break;
                case ModelType.SmallWorldAddLinksPreferably:
                    model = new ModelSmallWorldAddLinksPreferably();
                    ((ModelSmallWorldAddLinksPreferably)model).addProb = prob;
                    break;

                case ModelType.ErdosRenyiRewiring:
                    model = new ModelErdosRenyiRewiring();
                    ((ModelErdosRenyiRewiring)model).rewireProb = prob;
                    break;

                default:
                    return null;
            }

            model.n = m.n;
            model.kT = m.kT;
            model.iter = m.iter;
            return model;
        }
        private Model CreateModel(Model m)
        {
            double prob;
            switch (m.modelType)
            {
                case ModelType.SmallWorldRewiringRandomly:
                    prob = ((ModelSmallWorldRewiringRandomly)m).rewireProb;
                    break;
                case ModelType.SmallWorldRewiringPreferably:
                    prob = ((ModelSmallWorldRewiringPreferably)m).rewireProb;
                    break;
                case ModelType.SmallWorldAddLinksRandomly:
                    prob = ((ModelSmallWorldAddLinksRandomly)m).addProb;
                    break;
                case ModelType.SmallWorldAddLinksPreferably:
                    prob = ((ModelSmallWorldAddLinksPreferably)m).addProb;
                    break;

                case ModelType.ErdosRenyiRewiring:
                    prob = ((ModelErdosRenyiRewiring)m).rewireProb;
                    break;

                default:
                    prob = 0;
                    break;
            }

            return CreateModel(m, prob);
        }
        

        private void CreateIsingCalculation()
        {
            switch (model.modelType)
            {
                case ModelType.RegularLattice:
                    calc = new IsingRegularLattice(model, this);
                    break;
                case ModelType.SmallWorldRewiringRandomly:
                    calc = new IsingSmallWorldRewiringRandomly(model, this);
                    break;
                case ModelType.SmallWorldRewiringPreferably:
                    calc = new IsingSmallWorldRewiringPreferably(model, this);
                    break;
                case ModelType.SmallWorldAddLinksRandomly:
                    calc = new IsingSmallWorldAddLinksRandomly(model, this);
                    break;
                case ModelType.SmallWorldAddLinksPreferably:
                    calc = new IsingSmallWorldAddLinksPreferably(model, this);
                    break;

                case ModelType.ErdosRenyiRewiring:
                    calc = new ErdosRenyiRewiring(model, this);
                    break;

                default:
                    model = null;
                    break;
            }
        }

        public void AddShortestPathForExport(double prob, double avgPath)
        {
            probToAveragePath[prob] = avgPath;
        }

        private async void buttonGenerate_Click(object sender, EventArgs e)
        {
            mode = Mode.Generate;
            InitState();

            run++;
            CreateModel();
            CreateIsingCalculation();

            cancelTokenSource = new CancellationTokenSource();
            var cancelToken = cancelTokenSource.Token;

            try
            {
                task = Task.Run(() =>
                {
                    if (onlyQueryPath)
                    {
                        QueryPath(cancelToken);
                        return;
                    }

                    // run monte-carlo
                    calc.GenerateTopology(cancelToken);
                    calc.Generate(cancelToken);
                    calc.BuildCorrelationMatrix(cancelToken);
                    corr = calc.corr;
                    WriteCorrelationsToFile(corr);
                },
                cancelToken);
                await task;
            }
            catch (OperationCanceledException)
            { 
            }
            finally
            {
                task = null;
                mode = Mode.None;

                if (!cancelTokenSource.IsCancellationRequested)
                    generatedOnce = true;
                InitState();

                if (!cancelTokenSource.IsCancellationRequested)
                    MessageBox.Show("Done!");

            }
        }

        private void QueryPath(CancellationToken cancelToken)
        {
            probToAveragePath = new Dictionary<double, double>();
            Model modelTmp = CreateModel(model);

            var probList = new List<double>();
            probList.Add(0.000025);
            probList.Add(0.00005);
            probList.Add(0.000075);
            for (double prob = 0.0001; prob <= 0.0004; prob += 0.0001)
                probList.Add(prob);
            for (double prob = 0.0008; prob <= 0.0016; prob += 0.0004)
                probList.Add(prob);
            for (double prob = 0.002; prob <= 0.008; prob += 0.002)
                probList.Add(prob);
            for (double prob = 0.01; prob <= 0.09; prob += 0.01)
                probList.Add(prob);
            for (double prob = 0.1; prob <= 1.0; prob += 0.1)
                probList.Add(prob);

            foreach (var prob in probList)
            {
                model = CreateModel(modelTmp, prob);
                CreateIsingCalculation();
                calc.GenerateTopology(cancelToken);
            }

            CreateDir();
            string file = dir + "\\paths_" + run.ToString() + ".csv";
            PathWriter.WriteToFile(model, probToAveragePath, file);
        }


        private async void buttonProcess_Click(object sender, EventArgs e)
        {
            mode = Mode.Process;
            InitState();

            cancelTokenSource = new CancellationTokenSource();
            var cancelToken = cancelTokenSource.Token;

            try
            {
                string corrFile = "";
                bool needToLoadCorrelations = (!checkBox1.Checked || corr == null);

                if (needToLoadCorrelations)
                {
                    corrFile = PickCorrelationsFile();
                    if (corrFile == "")
                        return;
                }

                string outFile = dir + "\\run_" + run.ToString() + ".csv";
                double threshold = (double)numericUpDown4.Value;

                bool needToFindThreshold = checkBox2.Checked;
                int avDegReq = Convert.ToInt32(numericUpDown5.Value);

                task = Task.Run(() =>
                {
                    if (needToLoadCorrelations)
                    {
                        if (corrFile.Substring(corrFile.Length - 4).ToLower() == ".csv")
                            corr = CorrelationsTextParser.ReadFromFile(corrFile);
                        else
                            corr = CorrelationsBinaryParser.ReadFromFile(corrFile, this);
                    }
                    NetworkProcessor proc = new NetworkProcessor(corr, this);

                    if (needToFindThreshold)
                        proc.FindThreshold(avDegReq, cancelToken);
                    else
                        proc.Process(threshold, outFile, cancelToken);
                },
                cancelToken);
                await task;
            }
            catch (OperationCanceledException)
            {
            }
            finally
            {
                task = null;
                mode = Mode.None;
                InitState();

                run++;
                checkBox1.Enabled = true;
                MessageBox.Show("Done!");
            }
        }


        private void buttonAbort_Click(object sender, EventArgs e)
        {
            cancelTokenSource.Cancel();
        }


        private string PickCorrelationsFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (dirCreated)
                openFileDialog.InitialDirectory = dir;
            else
                openFileDialog.InitialDirectory = mainDir;

            openFileDialog.Filter = "COF files (*.cof)|*.cof|CSV files (*.csv)|*.csv";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
                return openFileDialog.FileName;
            return "";
        }

        public void CreateDir()
        {
            if (!dirCreated)
            {
                dirCreated = true;
                Directory.CreateDirectory(dir);
            }
        }

        private void WriteCorrelationsToFile(Correlations corr)
        {
            CreateDir();
            //string file = dir + "\\corr_" + run.ToString() + ".csv";
            //CorrelationsTextParser.WriteToFile(corr, file);

            string filebin = dir + "\\corr_" + run.ToString() + ".cof";
            CorrelationsBinaryParser.WriteToFile(corr, filebin);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (task != null)
                cancelTokenSource.Cancel();
        }
    }
}
