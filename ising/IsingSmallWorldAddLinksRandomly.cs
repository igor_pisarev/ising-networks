﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ising
{
    public class IsingSmallWorldAddLinksRandomly : IsingRegularLattice
    {
        public IsingSmallWorldAddLinksRandomly(Model model, Form1 callbacks)
            : base(model, callbacks)
        { }

        public override void GenerateTopology(CancellationToken cancelToken)
        {
            base.GenerateTopology(cancelToken);

            int nodes = MathUtils.Sqr(model.n);
            double addProb = (model as ModelSmallWorldAddLinksRandomly).addProb;

            // add new random links
            for (int i = 0; i < nodes; i++)
            {
                if (!MathUtils.RandomTry(addProb))
                    continue;

                if (topology[i].Count == nodes - 1)
                    continue;

                int nodeAddLink;
                do
                    nodeAddLink = StaticRandom.Next(nodes);
                while (nodeAddLink == i || topology[i].Contains(nodeAddLink));

                topology[i].Add(nodeAddLink);
                topology[nodeAddLink].Add(i);

                cancelToken.ThrowIfCancellationRequested();
            }
        }
    }
}
