﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ising
{
    public class ErdosRenyiRewiring : IsingRegularLattice
    {
        public ErdosRenyiRewiring(Model model, Form1 callbacks)
            : base(model, callbacks)
        { }

        public override void GenerateTopology(CancellationToken cancelToken)
        {
            int n = model.n;
            int nodes = MathUtils.Sqr(n);

            // init
            topology = new HashSet<int>[nodes];
            for (int i = 0; i < nodes; i++)
                topology[i] = new HashSet<int>();

            int edges = 2 * nodes;
            for (int e = 0; e < edges; e++)
            {
                int u, v;
                do
                {
                    u = StaticRandom.Next(nodes);
                    v = StaticRandom.Next(nodes);
                }
                while (u == v || topology[u].Contains(v));

                topology[u].Add(v);
                topology[v].Add(u);
            }
            

            double rewireProb = (model as ModelErdosRenyiRewiring).rewireProb;

            // rewire
            for (int i = 0; i < nodes; i++)
            {
                if (!MathUtils.RandomTry(rewireProb))
                    continue;

                if (topology[i].Count == 0 || topology[i].Count == nodes - 1)
                    continue;

                int nodeRemoveLink = topology[i].ElementAt(StaticRandom.Next(topology[i].Count));
                int nodeAddLink;
                do
                    nodeAddLink = StaticRandom.Next(nodes);
                while (nodeAddLink == i || nodeAddLink == nodeRemoveLink || topology[i].Contains(nodeAddLink));

                topology[i].Remove(nodeRemoveLink);
                topology[nodeRemoveLink].Remove(i);

                topology[i].Add(nodeAddLink);
                topology[nodeAddLink].Add(i);

                cancelToken.ThrowIfCancellationRequested();
            }

            CheckTopology(rewireProb, cancelToken);
        }
    }
}
