﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace ising
{
    public class Correlations
    {
        public Model model;
        public int nodes;
        public SquareSymmetricArray<float> correlations;

        public List<float> corrList;

        public Correlations() { }
        public Correlations(Model model, int nodes, SquareSymmetricArray<float> correlations = null)
        {
            this.model = model;
            this.nodes = nodes;
            this.correlations = correlations ?? new SquareSymmetricArray<float>(nodes);
            corrList = null;
        }
    }


    public static class PathWriter
    {
        public static void WriteToFile(Model model, Dictionary<double, double> p, string file)
        {
            using (FileStream fs = new FileStream(file, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("model;" + model.modelType.ToString());
                    sw.WriteLine("nodes;" + MathUtils.Sqr(model.n).ToString());
                    sw.WriteLine();
                    sw.WriteLine("prob;path");
                    foreach (var item in p)
                        sw.WriteLine(item.Key.ToString() + ";" + item.Value.ToString());
                }
            }
        }
    }


    public static class CorrelationsTextParser
    {
        public static void WriteToFile(Correlations c, string file)
        {
            using (FileStream fs = new FileStream(file, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(c.model.ToString());
                    sw.WriteLine(c.nodes);

                    for (int i = 0; i < c.nodes; i++)
                    {
                        bool delimNeeded = false;
                        for (int j = i + 1; j < c.nodes; j++)
                        {
                            if (delimNeeded)
                                sw.Write(';');
                            delimNeeded = true;

                            sw.Write(c.correlations.Get(i, j));
                        }
                        sw.WriteLine();
                    }
                }
            }
        }

        public static Correlations ReadFromFile(string file)
        {
            Correlations c = new Correlations();
            using (FileStream fs = new FileStream(file, FileMode.Open))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        c.model = ModelParser.Parse(sr.ReadLine());
                        c.nodes = Convert.ToInt32(sr.ReadLine());

                        c.correlations = new SquareSymmetricArray<float>(c.nodes);
                        for (int i = 0; i < c.nodes; i++)
                        {
                            string[] data = sr.ReadLine().Split(';');
                            int counter = 0;
                            for (int j = i + 1; j < c.nodes; j++)
                            {
                                c.correlations.Put(i, j, Convert.ToSingle(data[counter]));
                                counter++;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    MessageBox.Show(ex.InnerException.ToString());
                }
            }
            return c;
        }
    }



    public static class CorrelationsBinaryParser
    {
        public static void WriteToFile(Correlations c, string file)
        {
            using (FileStream fs = new FileStream(file, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    c.model.WriteToBinary(bw);
                    bw.Write(c.nodes);

                    for (int i = 0; i < c.nodes; i++)
                        for (int j = i + 1; j < c.nodes; j++)
                            bw.Write(c.correlations.Get(i, j));
                }
            }
        }

        public static Correlations ReadFromFile(string file, Form1 callbacks)
        {
            Correlations c = new Correlations();
            using (FileStream fs = new FileStream(file, FileMode.Open))
            {
                try
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        c.model = ModelParser.Parse(br);
                        c.nodes = br.ReadInt32();

                        c.correlations = new SquareSymmetricArray<float>(c.nodes);

                        callbacks.Invoke(callbacks.actionChangeStatus, "Reading data...");
                        callbacks.Invoke(callbacks.actionInitProgress, c.nodes);

                        for (int i = 0; i < c.nodes; i++)
                        {
                            for (int j = i + 1; j < c.nodes; j++)
                                c.correlations.Put(i, j, br.ReadSingle());

                            callbacks.Invoke(callbacks.actionIncProgress);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    MessageBox.Show(ex.InnerException.ToString());
                }
            }
            return c;
        }
    }
}
