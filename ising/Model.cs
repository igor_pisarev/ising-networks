﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ising
{
    public enum ModelType
    {
        RegularLattice,
        SmallWorldRewiringRandomly,
        SmallWorldAddLinksRandomly,
        SmallWorldAddLinksPreferably,
        SmallWorldRewiringPreferably,

        ErdosRenyiRewiring,
    }


    public abstract class Model
    {
        public static Dictionary<ModelType, string> ModelDescription = new Dictionary<ModelType, string>
        {
            { ModelType.RegularLattice, "2D regular lattice" },
            { ModelType.SmallWorldRewiringRandomly, "Small-world (rewiring randomly)" },
            { ModelType.SmallWorldRewiringPreferably, "Small-world (rewiring preferably)" },
            { ModelType.SmallWorldAddLinksRandomly, "Small-world (add links randomly)" },
            { ModelType.SmallWorldAddLinksPreferably, "Small-world (add links preferably)" },

            { ModelType.ErdosRenyiRewiring, "Erdos-Renyi (rewiring)" },
        };


        public ModelType modelType { get; protected set; }
        public int n;
        public double kT;
        public int iter;

        protected string[] data;
        protected int counter;

        public Model(ModelType modelType)
        {
            this.modelType = modelType;
        }

        public Model(ModelType modelType, int n, double kT, int iter)
        {
            this.modelType = modelType;
            this.n = n;
            this.kT = kT;
            this.iter = iter;
        }

        public Model(ModelType modelType, string s)
        {
            this.modelType = modelType;

            data = s.Split(';');
            counter = 1;
            this.n = Convert.ToInt32(data[counter++]);
            this.kT = Convert.ToDouble(data[counter++]);
            this.iter = Convert.ToInt32(data[counter++]);
        }

        public Model(ModelType modelType, BinaryReader br)
        {
            this.modelType = modelType;
            this.n = br.ReadInt32();
            this.kT = br.ReadDouble();
            this.iter = br.ReadInt32();
        }


        public override string ToString()
        {
            return ((int)modelType).ToString() + ";" + n.ToString() + ";" + kT.ToString() + ";" + iter.ToString();
        }


        public virtual void WriteToBinary(BinaryWriter bw)
        {
            bw.Write((int)modelType);
            bw.Write(n);
            bw.Write(kT);
            bw.Write(iter);
        }
    }


    public class ModelRegularLattice : Model
    {
        public ModelRegularLattice()
            : base(ModelType.RegularLattice)
        { }

        public ModelRegularLattice(int n, double kT, int iter)
            : base(ModelType.RegularLattice, n, kT, iter)
        { }

        public ModelRegularLattice(string s)
            : base(ModelType.RegularLattice, s)
        { }

        public ModelRegularLattice(BinaryReader br)
            : base(ModelType.RegularLattice, br)
        { }
    }


    public class ModelSmallWorldRewiringRandomly : Model
    {
        public double rewireProb;

        public ModelSmallWorldRewiringRandomly()
            : base(ModelType.SmallWorldRewiringRandomly)
        { }

        public ModelSmallWorldRewiringRandomly(double rewireProb, int n, double kT, int iter)
            : base(ModelType.SmallWorldRewiringRandomly, n, kT, iter)
        {
            this.rewireProb = rewireProb;
        }

        public ModelSmallWorldRewiringRandomly(string s)
            : base(ModelType.SmallWorldRewiringRandomly, s)
        {
            this.rewireProb = Convert.ToDouble(data[counter++]);
        }

        public ModelSmallWorldRewiringRandomly(BinaryReader br)
            : base(ModelType.SmallWorldRewiringRandomly, br)
        {
            this.rewireProb = br.ReadDouble();
        }

        public override string ToString()
        {
            return base.ToString() + ";" + rewireProb.ToString();
        }

        public override void WriteToBinary(BinaryWriter bw)
        {
            base.WriteToBinary(bw);
            bw.Write(rewireProb);
        }
    }


    public class ModelSmallWorldRewiringPreferably : Model
    {
        public double rewireProb;

        public ModelSmallWorldRewiringPreferably()
            : base(ModelType.SmallWorldRewiringPreferably)
        { }

        public ModelSmallWorldRewiringPreferably(double rewireProb, int n, double kT, int iter)
            : base(ModelType.SmallWorldRewiringPreferably, n, kT, iter)
        {
            this.rewireProb = rewireProb;
        }

        public ModelSmallWorldRewiringPreferably(string s)
            : base(ModelType.SmallWorldRewiringPreferably, s)
        {
            this.rewireProb = Convert.ToDouble(data[counter++]);
        }

        public ModelSmallWorldRewiringPreferably(BinaryReader br)
            : base(ModelType.SmallWorldRewiringPreferably, br)
        {
            this.rewireProb = br.ReadDouble();
        }

        public override string ToString()
        {
            return base.ToString() + ";" + rewireProb.ToString();
        }

        public override void WriteToBinary(BinaryWriter bw)
        {
            base.WriteToBinary(bw);
            bw.Write(rewireProb);
        }
    }


    public class ModelSmallWorldAddLinksRandomly : Model
    {
        public double addProb;

        public ModelSmallWorldAddLinksRandomly()
            : base(ModelType.SmallWorldAddLinksRandomly)
        { }

        public ModelSmallWorldAddLinksRandomly(double addProb, int n, double kT, int iter)
            : base(ModelType.SmallWorldAddLinksRandomly, n, kT, iter)
        {
            this.addProb = addProb;
        }

        public ModelSmallWorldAddLinksRandomly(string s)
            : base(ModelType.SmallWorldAddLinksRandomly, s)
        {
            this.addProb = Convert.ToDouble(data[counter++]);
        }

        public ModelSmallWorldAddLinksRandomly(BinaryReader br)
            : base(ModelType.SmallWorldAddLinksRandomly, br)
        {
            this.addProb = br.ReadDouble();
        }

        public override string ToString()
        {
            return base.ToString() + ";" + addProb.ToString();
        }

        public override void WriteToBinary(BinaryWriter bw)
        {
            base.WriteToBinary(bw);
            bw.Write(addProb);
        }
    }


    public class ModelSmallWorldAddLinksPreferably : Model
    {
        public double addProb;

        public ModelSmallWorldAddLinksPreferably()
            : base(ModelType.SmallWorldAddLinksPreferably)
        { }

        public ModelSmallWorldAddLinksPreferably(double addProb, int n, double kT, int iter)
            : base(ModelType.SmallWorldAddLinksPreferably, n, kT, iter)
        {
            this.addProb = addProb;
        }

        public ModelSmallWorldAddLinksPreferably(string s)
            : base(ModelType.SmallWorldAddLinksPreferably, s)
        {
            this.addProb = Convert.ToDouble(data[counter++]);
        }

        public ModelSmallWorldAddLinksPreferably(BinaryReader br)
            : base(ModelType.SmallWorldAddLinksPreferably, br)
        {
            this.addProb = br.ReadDouble();
        }

        public override string ToString()
        {
            return base.ToString() + ";" + addProb.ToString();
        }

        public override void WriteToBinary(BinaryWriter bw)
        {
            base.WriteToBinary(bw);
            bw.Write(addProb);
        }
    }



    public class ModelErdosRenyiRewiring : Model
    {
        public double rewireProb;

        public ModelErdosRenyiRewiring()
            : base(ModelType.ErdosRenyiRewiring)
        { }

        public ModelErdosRenyiRewiring(double rewireProb, int n, double kT, int iter)
            : base(ModelType.ErdosRenyiRewiring, n, kT, iter)
        {
            this.rewireProb = rewireProb;
        }

        public ModelErdosRenyiRewiring(BinaryReader br)
            : base(ModelType.ErdosRenyiRewiring, br)
        {
            this.rewireProb = br.ReadDouble();
        }

        public ModelErdosRenyiRewiring(string s)
            : base(ModelType.ErdosRenyiRewiring, s)
        {
            this.rewireProb = Convert.ToDouble(data[counter++]);
        }

        public override string ToString()
        {
            return base.ToString() + ";" + rewireProb.ToString();
        }
    }


    public static class ModelParser
    {
        public static Model Parse(string s)
        {
            ModelType type = (ModelType)(Convert.ToInt32(new String(s[0], 1)));
            switch (type)
            {
                case ModelType.RegularLattice:
                    return new ModelRegularLattice(s);
                case ModelType.SmallWorldRewiringRandomly:
                    return new ModelSmallWorldRewiringRandomly(s);
                case ModelType.SmallWorldRewiringPreferably:
                    return new ModelSmallWorldRewiringPreferably(s);
                case ModelType.SmallWorldAddLinksRandomly:
                    return new ModelSmallWorldAddLinksRandomly(s);
                case ModelType.SmallWorldAddLinksPreferably:
                    return new ModelSmallWorldAddLinksPreferably(s);

                case ModelType.ErdosRenyiRewiring:
                    return new ModelErdosRenyiRewiring(s);

                default:
                    throw new Exception("todo");
            }
        }

        public static Model Parse(BinaryReader br)
        {
            ModelType type = (ModelType)br.ReadInt32();
            switch (type)
            {
                case ModelType.RegularLattice:
                    return new ModelRegularLattice(br);
                case ModelType.SmallWorldRewiringRandomly:
                    return new ModelSmallWorldRewiringRandomly(br);
                case ModelType.SmallWorldRewiringPreferably:
                    return new ModelSmallWorldRewiringPreferably(br);
                case ModelType.SmallWorldAddLinksRandomly:
                    return new ModelSmallWorldAddLinksRandomly(br);
                case ModelType.SmallWorldAddLinksPreferably:
                    return new ModelSmallWorldAddLinksPreferably(br);

                case ModelType.ErdosRenyiRewiring:
                    return new ModelErdosRenyiRewiring(br);

                default:
                    throw new Exception("todo");
            }
        }
    }
}
