﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ising
{
    public static class StaticRandom
    {
        static Random rnd;
        public static void Init()
        {
            rnd = new Random();
        }

        public static int Next()
        {
            return rnd.Next();
        }
        public static int Next(int maxValue)
        {
            return rnd.Next(maxValue);
        }
        public static int Next(int minValue, int maxValue)
        {
            return rnd.Next(minValue, maxValue);
        }
        public static double NextDouble()
        {
            return rnd.NextDouble();
        }
    }


    public class SquareSymmetricArray<T>
    {
        T[][] arr;
        public SquareSymmetricArray(int n)
        {
            arr = new T[n][];
            for (int i = 0; i < n; i++)
                arr[i] = new T[n - i];
        }

        public T Get(int i, int j)
        {
            return arr[Math.Min(i, j)][Math.Abs(j - i)];
        }

        public void Put(int i, int j, T val)
        {
            arr[Math.Min(i, j)][Math.Abs(j - i)] = val;
        }
    }


    public class ConcurrentSquareSymmetricArray<T>
    {
        ConcurrentDictionary<int, T>[] arr;
        public ConcurrentSquareSymmetricArray(int n)
        {
            arr = new ConcurrentDictionary<int, T>[n];
            for (int i = 0; i < n; i++)
                arr[i] = new ConcurrentDictionary<int, T>();
        }

        public T Get(int i, int j)
        {
            return arr[Math.Min(i, j)][Math.Abs(j - i)];
        }

        public void Put(int i, int j, T val)
        {
            arr[Math.Min(i, j)][Math.Abs(j - i)] = val;
        }
    }


    public class Pair<T, U>
    {
        public T First;
        public U Second;

        public Pair()
        {
            First = default(T);
            Second = default(U);
        }
        public Pair(T First, U Second)
        {
            this.First = First;
            this.Second = Second;
        }
    }


    public static class MathUtils
    {
        public static void Multiply(int n, SquareSymmetricArray<int> a, SquareSymmetricArray<int> b, SquareSymmetricArray<int> res)
        {
            for (int i = 0; i < n; i++)
                for (int j = i; j < n; j++)
                    for (int k = 0; k < n; k++)
                        res.Put(i, j, res.Get(i, j) + a.Get(i, k) * b.Get(j, k));
        }

        public static void Multiplyii(int n, SquareSymmetricArray<int> a, SquareSymmetricArray<int> b, int[] res)
        {
            for (int i = 0; i < n; i++)
                for (int k = 0; k < n; k++)
                    res[i] += a.Get(i, k) * b.Get(i, k);
        }

        public static int ModN(int x, int n)
        {
            if (x < 0)
                return x + n;
            else if (x >= n)
                return x - n;
            else
                return x;
        }

        public static int CellToIndex(int row, int col, int n)
        {
            return n * row + col;
        }

        public static double Sqr(double x)
        {
            return x * x;
        }
        public static int Sqr(int x)
        {
            return x * x;
        }

        public static bool RandomTry(double p)
        {
            double r = StaticRandom.NextDouble();
            return (r.CompareTo(p) <= 0 && p.CompareTo(0) > 0);
        }
    }
}
