﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ising
{
    public class IsingRegularLattice : IsingCalculation
    {
        public IsingRegularLattice(Model model, Form1 callbacks)
            : base(model, callbacks)
        { }

        public override void Generate(CancellationToken cancelToken)
        {
            callbacks.Invoke(callbacks.actionChangeStatus, "Generating...");

            int nodes = MathUtils.Sqr(model.n);
            int iter = model.iter;

            values = new int[nodes];
            ts = new int[nodes, iter + 1];

            //GenerateTopology(cancelToken);

            // initial configuration
            for (int i = 0; i < nodes; i++)
            {
                values[i] = (StaticRandom.Next(2) == 0 ? -1 : 1);
                ts[i, 0] = values[i];
            }

            callbacks.Invoke(callbacks.actionInitProgress, iter);
            callbacks.Invoke(callbacks.actionVisInit);
            callbacks.Invoke(callbacks.actionVisDraw);

            // monte-carlo
            for (int timestep = 1; timestep <= iter; timestep++)
            {
                // loop through all the nodes per one timestep
                for (int i = 0; i < nodes; i++)
                {
                    // nearest neighbours
                    int nn = 0;
                    foreach (int j in topology[i])
                        nn += values[j];

                    double dE = (double)(2 * nn * values[i]);
                    double p;
                    if (dE > 0)  // calculate probability
                        p = Math.Exp((-1.0 / model.kT) * dE);
                    else
                        p = 1.0;

                    if (MathUtils.RandomTry(p))  // flip
                        values[i] *= -1;

                    // slowing down tremendously
                    //callbacks.Invoke(callbacks.actionVisDrawCell, i, j);
                }

                for (int i = 0; i < nodes; i++)
                    ts[i, timestep] = values[i];

                cancelToken.ThrowIfCancellationRequested();
                callbacks.Invoke(callbacks.actionIncProgress);
                callbacks.Invoke(callbacks.actionVisDraw);
            }
        }

        public override void BuildCorrelationMatrix(CancellationToken cancelToken)
        {
            BuildCorrelationMatrixParallel(cancelToken);
        }



        public override void BuildCorrelationMatrixParallel(CancellationToken cancelToken)
        {
            base.BuildCorrelationMatrixParallel(cancelToken);

            int iter = model.iter;
            int nodes = corr.nodes;

            int total = Environment.ProcessorCount;

            callbacks.Invoke(callbacks.actionChangeStatus, "Building network...");
            callbacks.Invoke(callbacks.actionInitProgress, nodes);

            int[] sum = new int[nodes];
            int[] sum2 = new int[nodes];
            double[] av = new double[nodes];
            double[] av2 = new double[nodes];

            for (int i = 0; i < nodes; i++)
            {
                for (int k = 0; k <= iter; k++)
                {
                    sum[i] += ts[i, k];
                    sum2[i] += MathUtils.Sqr(ts[i, k]);
                }
            }

            for (int i = 0; i < nodes; i++)
            {
                av[i] = (double)sum[i] / (iter + 1);
                av2[i] = (double)sum2[i] / (iter + 1);
            }

            List<Task> tasks = new List<Task>();
            for (int id = 0; id < total; id++)
            {
                int idCurr = id;
                tasks.Add(Task.Run(() => BuildNetworkParallel(idCurr, total, av, av2, cancelToken)));
            }
            Task.WaitAll(tasks.ToArray());
        }

        private void BuildNetworkParallel(int id, int total, double[] av, double[] av2, CancellationToken cancelToken)
        {
            int iter = model.iter;
            int nodes = corr.nodes;

            for (int c1 = 0; c1 < nodes; c1++)
            {
                for (int c2 = c1 + 1 + id; c2 < nodes; c2 += total)
                {
                    int sum_product = 0;
                    for (int k = 0; k <= iter; k++)
                        sum_product += ts[c1, k] * ts[c2, k];

                    double av_product = (double)sum_product / (iter + 1);

                    double correlation =
                        (av_product - av[c1] * av[c2])
                                            /
                        Math.Sqrt((av2[c1] - MathUtils.Sqr(av[c1])) * (av2[c2] - MathUtils.Sqr(av[c2])));

                    corr.correlations.Put(c1, c2, (float)correlation);
                }

                if (id == 0)
                {
                    callbacks.Invoke(callbacks.actionIncProgress);
                    cancelToken.ThrowIfCancellationRequested();
                }
                else
                {
                    if (cancelToken.IsCancellationRequested)
                        return;
                }
            }
        }




        public override void GenerateTopology(CancellationToken cancelToken)
        {
            int n = model.n;
            int nodes = MathUtils.Sqr(n);

            // init
            topology = new HashSet<int>[nodes];
            for (int i = 0; i < nodes; i++)
                topology[i] = new HashSet<int>();

            int ind = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    int down  = MathUtils.CellToIndex(MathUtils.ModN(i + 1, n), j, n);
                    int right = MathUtils.CellToIndex(i, MathUtils.ModN(j + 1, n), n);
                    topology[ind].Add(down);
                    topology[ind].Add(right);
                    topology[down].Add(ind);
                    topology[right].Add(ind);
                    ind++;
                }
            }

            cancelToken.ThrowIfCancellationRequested();
        }
    }
}
