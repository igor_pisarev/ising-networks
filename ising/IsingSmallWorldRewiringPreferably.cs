﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ising
{
    public class IsingSmallWorldRewiringPreferably : IsingRegularLattice
    {
        // preferential attachment O(N)
        int deg_sum;

        public IsingSmallWorldRewiringPreferably(Model model, Form1 callbacks)
            : base(model, callbacks)
        { }

        public override void GenerateTopology(CancellationToken cancelToken)
        {
            callbacks.Invoke(callbacks.actionChangeStatus, "Constructing topology...");
            
            base.GenerateTopology(cancelToken);

            int nodes = MathUtils.Sqr(model.n);
            double rewireProb = (model as ModelSmallWorldRewiringPreferably).rewireProb;

            callbacks.Invoke(callbacks.actionInitProgress, nodes);

            // init preferential attachment
            deg_sum = 0;
            for (int i = 0; i < nodes; i++)
                deg_sum += topology[i].Count;

            // rewire links with preferential attachment
            for (int i = 0; i < nodes; i++)
            {
                if (!MathUtils.RandomTry(rewireProb))
                    continue;

                if (topology[i].Count == nodes - 1)
                    continue;

                int nodeRemoveLink = topology[i].ElementAt(StaticRandom.Next(topology[i].Count));
                int nodeAddLink;
                do
                    nodeAddLink = ChooseNodePreferably();
                while (nodeAddLink == i || nodeAddLink == nodeRemoveLink || topology[i].Contains(nodeAddLink));

                topology[i].Remove(nodeRemoveLink);
                topology[nodeRemoveLink].Remove(i);

                topology[i].Add(nodeAddLink);
                topology[nodeAddLink].Add(i);

                cancelToken.ThrowIfCancellationRequested();
                callbacks.Invoke(callbacks.actionIncProgress);
            }

            CheckTopology(rewireProb, cancelToken);
        }

        public int ChooseNodePreferably()
        {
            int nodes = topology.Length;
            int rnd = StaticRandom.Next(deg_sum);
            int sum = 0;
            int curr;
            for (curr = 0; curr < nodes; curr++)
            {
                if (sum + topology[curr].Count > rnd)
                    break;

                sum += topology[curr].Count;
            }
            return curr;
        }
    }
}
