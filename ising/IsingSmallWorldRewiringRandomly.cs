﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ising
{
    public class IsingSmallWorldRewiringRandomly : IsingRegularLattice
    {
        public IsingSmallWorldRewiringRandomly(Model model, Form1 callbacks)
            : base(model, callbacks)
        { }

        public override void GenerateTopology(CancellationToken cancelToken)
        {
 	        base.GenerateTopology(cancelToken);

            int nodes = MathUtils.Sqr(model.n);
            double rewireProb = (model as ModelSmallWorldRewiringRandomly).rewireProb;

            // rewire
            for (int i = 0; i < nodes; i++)
            {
                if (!MathUtils.RandomTry(rewireProb))
                    continue;

                if (topology[i].Count == nodes - 1)
                    continue;

                int nodeRemoveLink = topology[i].ElementAt(StaticRandom.Next(topology[i].Count));
                int nodeAddLink;
                do
                    nodeAddLink = StaticRandom.Next(nodes);
                while (nodeAddLink == i || nodeAddLink == nodeRemoveLink || topology[i].Contains(nodeAddLink));

                topology[i].Remove(nodeRemoveLink);
                topology[nodeRemoveLink].Remove(i);

                topology[i].Add(nodeAddLink);
                topology[nodeAddLink].Add(i);

                cancelToken.ThrowIfCancellationRequested();
            }

            CheckTopology(rewireProb, cancelToken);
        }
    }
}
