﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ising
{
    public class NetworkProcessor
    {
        const int INFINITY = Int32.MaxValue;

        Form1 callbacks;
        Correlations corr;
        int[,] lengths;

        Object[] locks;
        HashSet<int>[] adjSet;
        Dictionary<int, HashSet<int>> adjCompSet;
        List<int> maxCompNodes;

        SortedDictionary<int, int> pk;
        SortedDictionary<int, double> ck;
        
        int edgesCount;
        int componentsCount, nonTrivialComponentsCount;
        double averageDeg;
        List<Pair<int, double>> averageShortestPathInNonTrivialComponents;
        double averageShortestPath;
        int diameter;
        double clusteringCoeff;
        Dictionary<int, double> ci;
        double density;
        double threshold;
        string filename;

        public NetworkProcessor(Correlations corr, Form1 callbacks)
        {
            this.corr = corr;
            this.callbacks = callbacks;
        }

        public NetworkProcessor(HashSet<int>[] adjSet, Form1 callbacks)
        {
            this.adjSet = adjSet;
            this.callbacks = callbacks;
        }

        public void ProcessTopology(CancellationToken cancelToken)
        {
            CalculateAdditionalProperties();
            CalculateAverageShortestPathLength(cancelToken);
            CalculateClusteringCoefficient(cancelToken);
            string s = "clustering = " + clusteringCoeff.ToString() + Environment.NewLine + "path = " + averageShortestPath.ToString();
            MessageBox.Show(s);
        }

        public double ProcessAverageShortestLengthPath(CancellationToken cancelToken)
        {
            CalculateAverageShortestPathLength(cancelToken);
            return averageShortestPath;
        }

        
        public void FindThreshold(int avDegReq, CancellationToken cancelToken)
        {
            // TODO
            /*int nodes = corr.nodes;

            if (corr.corrList == null)
            {
                callbacks.Invoke(callbacks.actionChangeStatus, "Sorting correlations...");
                callbacks.Invoke(callbacks.actionInitProgress, nodes);

                corr.corrList = new List<float>();
                float co;
                for (int i = 0; i < nodes; i++)
                {
                    for (int j = i + 1; j < nodes; j++)
                    {
                        co = corr.correlations.Get(i, j);
                        if (co >= 0)
                            corr.corrList.Add(co);
                    }

                    callbacks.Invoke(callbacks.actionIncProgress);
                }

                corr.corrList.Sort();
            }
            */
        }

        public void Process(double threshold, string filename, CancellationToken cancelToken)
        {
            this.threshold = threshold;
            this.filename = filename;

            MakeAdjacencyMatrix(cancelToken);
            Calculate(cancelToken);
            Export(cancelToken);
        }

        void MakeAdjacencyMatrix(CancellationToken cancelToken)
        {
            int nodes = corr.nodes;

            int total = Environment.ProcessorCount;

            callbacks.Invoke(callbacks.actionChangeStatus, "Building graph...");
            callbacks.Invoke(callbacks.actionInitProgress, nodes);

            adjSet = new HashSet<int>[nodes];
            for (int i = 0; i < nodes; i++)
                adjSet[i] = new HashSet<int>();

            locks = new Object[nodes];
            for (int i = 0; i < nodes; i++)
                locks[i] = new Object();

            List<Task> tasks = new List<Task>();
            for (int id = 0; id < total; id++)
            {
                int idCurr = id;
                tasks.Add(Task.Run(() => MakeAdjacencyMatrixParallel(idCurr, total, cancelToken)));
            }
            Task.WaitAll(tasks.ToArray());

            CalculateAdditionalProperties();
        }
        private void MakeAdjacencyMatrixParallel(int id, int total, CancellationToken cancelToken)
        {
            int nodes = corr.nodes;
            for (int i = 0; i < nodes; i++)
            {
                for (int j = i + 1 + id; j < nodes; j += total)
                {
                    bool needAdd = false;
                    double x = corr.correlations.Get(i, j);
                    if (Math.Abs(threshold).CompareTo(0) == 0)
                        needAdd = true; // make complete graph

                    if (Math.Abs(x).CompareTo(Math.Abs(threshold)) >= 0 &&
                            Math.Sign(x) == Math.Sign(threshold))
                        needAdd = true;

                    if (needAdd)
                    {
                        lock (locks[i])
                            adjSet[i].Add(j);

                        lock (locks[j])
                            adjSet[j].Add(i);
                    }
                }

                if (id == 0)
                {
                    callbacks.Invoke(callbacks.actionIncProgress);
                    cancelToken.ThrowIfCancellationRequested();
                }
                else
                {
                    if (cancelToken.IsCancellationRequested)
                        return;
                }
            }
        }

        private void CalculateAdditionalProperties()
        {
            int nodes = adjSet.Length;
            edgesCount = 0;
            for (int i = 0; i < nodes; i++)
                edgesCount += adjSet[i].Count;
            edgesCount /= 2;
            density = (double)edgesCount / ((nodes * (nodes - 1)) / 2);
        }


        private void Calculate(CancellationToken cancelToken)
        {
            callbacks.Invoke(callbacks.actionChangeStatus, "Calculating properties...");
            callbacks.Invoke(callbacks.actionInitProgress, (adjSet.Length * (adjSet.Length - 1)) / 2);

            CalculateAverageShortestPathLength(cancelToken);
            CalculateDegreeDistribution(cancelToken);
            CalculateClusteringCoefficient(cancelToken);
        }

        private void Export(CancellationToken cancelToken)
        {
            callbacks.CreateDir();
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("Input parameters:");
                    sw.WriteLine("model;{0}", corr.model.modelType.ToString());

                    switch (corr.model.modelType)
                    {
                        case ModelType.SmallWorldRewiringRandomly:
                            sw.WriteLine("prob;{0}", (corr.model as ModelSmallWorldRewiringRandomly).rewireProb.ToString());
                            break;
                        case ModelType.SmallWorldRewiringPreferably:
                            sw.WriteLine("prob;{0}", (corr.model as ModelSmallWorldRewiringPreferably).rewireProb.ToString());
                            break;
                        case ModelType.SmallWorldAddLinksRandomly:
                            sw.WriteLine("prob;{0}", (corr.model as ModelSmallWorldAddLinksRandomly).addProb.ToString());
                            break;
                        case ModelType.SmallWorldAddLinksPreferably:
                            sw.WriteLine("prob;{0}", (corr.model as ModelSmallWorldAddLinksPreferably).addProb.ToString());
                            break;
                        default:
                            break;
                    }
                       
                    sw.WriteLine("lattice;{0}", corr.model.n);
                    sw.WriteLine("nodes;{0}", corr.nodes);
                    sw.WriteLine("iterations;{0}", corr.model.iter);
                    sw.WriteLine();
                    sw.WriteLine("temperature;{0}", corr.model.kT);
                    sw.WriteLine("threshold;{0}", threshold);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.WriteLine("Calculated properties:");
                    sw.WriteLine("edges;{0}", edgesCount);
                    sw.WriteLine("components;{0}", componentsCount);
                    sw.WriteLine("non trivial;{0}", nonTrivialComponentsCount);
                    sw.WriteLine("density;{0}", density);
                    sw.WriteLine("average degree;{0}", averageDeg);
                    sw.WriteLine();
                    sw.WriteLine("av.shortest path;{0}", averageShortestPath);
                    sw.WriteLine("diameter;{0}", diameter);
                    sw.WriteLine("clustering;{0}", clusteringCoeff);
                    sw.WriteLine();
                    ExportAverageShortestPaths(sw);
                    sw.WriteLine();
                    ExportDegreeDistribution(sw);
                    sw.WriteLine();
                    ExportClusteringDistribution(sw);
                }
            }
        }

        private void CalculateDegreeDistribution(CancellationToken cancelToken)
        {
            pk = new SortedDictionary<int, int>();
            int degSum = 0;

            foreach (var item in adjCompSet)
            {
                int cnt = item.Value.Count;
                if (!pk.ContainsKey(cnt))
                    pk[cnt] = 0;
                pk[cnt]++;

                degSum += cnt;
            }

            averageDeg = (double)degSum / adjCompSet.Count;
        }

        private void ExportDegreeDistribution(StreamWriter sw)
        {
            sw.WriteLine("Degree distribution:");
            foreach (var item in pk)
                sw.WriteLine("{0};{1}", item.Key, (double)item.Value / adjCompSet.Count);
        }

        private void ExportClusteringDistribution(StreamWriter sw)
        {
            sw.WriteLine("Clustering distribution:");
            foreach (var item in ck)
                sw.WriteLine("{0};{1}", item.Key, item.Value / pk[item.Key]);
        }

        private void ExportAverageShortestPaths(StreamWriter sw)
        {
            sw.WriteLine("Average shortest paths in components:");

            SortedDictionary<int, List<double>> sortedPaths = new SortedDictionary<int,List<double>>();
            foreach (var item in averageShortestPathInNonTrivialComponents)
            {
                if (!sortedPaths.ContainsKey(item.First))
                    sortedPaths[item.First] = new List<double>();
                sortedPaths[item.First].Add(item.Second);
            }

            List<Pair<int, double>> outList = new List<Pair<int, double>>();
            foreach (var item in sortedPaths)
                foreach (var val in item.Value)
                    outList.Add(new Pair<int, double>(item.Key, val));

            outList.Reverse();
            foreach (var item in outList)
                sw.WriteLine("{0};{1}", item.First, item.Second);
        }

        private void CalculateClusteringCoefficient(CancellationToken cancelToken)
        {
            int nodes = adjCompSet.Count;
            ci = new Dictionary<int, double>();
            ck = new SortedDictionary<int, double>();
            foreach (var node in maxCompNodes)
                ci[node] = 0;

            int total = Environment.ProcessorCount;
            callbacks.Invoke(callbacks.actionChangeStatus, "Calculating clustering coefficients...");
            callbacks.Invoke(callbacks.actionInitProgress, nodes);
            List<Task> tasks = new List<Task>();
            for (int id = 0; id < total; id++)
            {
                int idCurr = id;
                tasks.Add(Task.Run(() => CalculateClusteringCoefficientParallel(idCurr, total, cancelToken)));
            }
            Task.WaitAll(tasks.ToArray());
            cancelToken.ThrowIfCancellationRequested();

            clusteringCoeff = 0;
            foreach (var item in ci)
                clusteringCoeff += item.Value;
            clusteringCoeff /= nodes;

            foreach (var item in ci)
            {
                int cnt = adjCompSet[item.Key].Count;
                if (!ck.ContainsKey(cnt))
                    ck[cnt] = 0;
                ck[cnt] += item.Value;
            }
        }
        private void CalculateClusteringCoefficientParallel(int id, int total, CancellationToken cancelToken)
        {
            for (int nodeInd = id; nodeInd < maxCompNodes.Count; nodeInd += total)
            {
                int node = maxCompNodes[nodeInd];
                int neighbours = adjCompSet[node].Count;
                long linksTotal = (neighbours * (neighbours - 1)) / 2;
                if (linksTotal == 0)
                    continue;

                long linksPresent = 0;

                foreach (int i in adjCompSet[node])
                    foreach (int j in adjCompSet[node])
                        if (adjCompSet[i].Contains(j))
                            linksPresent++;
                linksPresent /= 2;

                ci[node] = (double)linksPresent / linksTotal;


                callbacks.Invoke(callbacks.actionIncProgress);
                if (id == 0)
                    cancelToken.ThrowIfCancellationRequested();
                else if (cancelToken.IsCancellationRequested)
                    return;
            }
        }

        private void CalculateAverageShortestPathLength(CancellationToken cancelToken)
        {
            int nodes = adjSet.Length;

            callbacks.Invoke(callbacks.actionChangeStatus, "Calculating average shortest paths length...");
            callbacks.Invoke(callbacks.actionInitProgress, nodes);

            lengths = new int[nodes, nodes];

            int total = Environment.ProcessorCount;
            List<Task> tasks = new List<Task>();
            for (int id = 0; id < total; id++)
            {
                int idCurr = id;
                tasks.Add(Task.Run(() => InitLengthsParallel(idCurr, total, cancelToken)));
            }
            Task.WaitAll(tasks.ToArray());
            cancelToken.ThrowIfCancellationRequested();
            
            
            tasks = new List<Task>();
            for (int id = 0; id < total; id++)
            {
                int idCurr = id;
                tasks.Add(Task.Run(() => CalculateAverageShortestPathLengthParallel(idCurr, total, cancelToken)));
            }
            Task.WaitAll(tasks.ToArray());
            cancelToken.ThrowIfCancellationRequested();


            // calculate average shortest path length in connected components independently
            // then take arithmetic mean of them
            var components = new List<List<int>>();
            bool[] belongs = new bool[nodes];
            for (int i = 0; i < nodes; i++)
            {
                if (belongs[i])
                    continue;

                belongs[i] = true;
                int componentInd = components.Count;
                components.Add(new List<int>());
                components[componentInd].Add(i);

                for (int j = 0; j < nodes; j++)
                {
                    if (i == j || lengths[i, j] == INFINITY)
                        continue;

                    belongs[j] = true;
                    components[componentInd].Add(j);
                }
            }

            cancelToken.ThrowIfCancellationRequested();

            averageShortestPathInNonTrivialComponents = new List<Pair<int,double>>();
            averageShortestPath = 0;
            componentsCount = components.Count;
            nonTrivialComponentsCount = 0;
            if (componentsCount == 0)
                return;

            int maxCompInd = 0;

            diameter = 0;
            for (int compInd = 0; compInd < components.Count; compInd++)
            {
                var comp = components[compInd];
                if (components[maxCompInd].Count < comp.Count)
                    maxCompInd = compInd;
                
                if (comp.Count < 2)
                    continue;

                nonTrivialComponentsCount++;
                long componentShortestPathSum = 0;
                for (int i = 0; i < comp.Count; i++)
                {
                    for (int j = i + 1; j < comp.Count; j++)
                    {
                        int l = lengths[comp[i], comp[j]];
                        componentShortestPathSum += l;

                        if (diameter < l)
                            diameter = l;
                    }
                }

                long pathsCount = (comp.Count * (comp.Count - 1)) / 2;
                double averageShortestPathInComp = (double)componentShortestPathSum / pathsCount;

                averageShortestPathInNonTrivialComponents.Add(new Pair<int, double>(comp.Count, averageShortestPathInComp));
                averageShortestPath += averageShortestPathInComp;

                cancelToken.ThrowIfCancellationRequested();
            }

            // free memory
            lengths = null;

            averageShortestPath /= nonTrivialComponentsCount;


            // giant component
            maxCompNodes = new List<int>();
            adjCompSet = new Dictionary<int, HashSet<int>>();
            var giantComp = components[maxCompInd];
            foreach (var node in giantComp)
            {
                maxCompNodes.Add(node);
                adjCompSet[node] = adjSet[node];
            }
        }

        private void InitLengthsParallel(int id, int total, CancellationToken cancelToken)
        {
            int nodes = adjSet.Length;
            for (int i = 0; i < nodes; i++)
                for (int j = i + 1 + id; j < nodes; j += total)
                    lengths[i, j] = lengths[j, i] = INFINITY;
        }

        private void CalculateAverageShortestPathLengthParallel(int id, int total, CancellationToken cancelToken)
        {
            for (int root = id; root < adjSet.Length; root += total)
            {
                Queue<int> queue = new Queue<int>();
                queue.Enqueue(root);
                while (queue.Count > 0)
                {
                    int curr = queue.Dequeue();

                    foreach (int next in adjSet[curr])
                    {
                        if (lengths[root, next] != INFINITY)
                            continue;

                        lengths[root, next] = lengths[root, curr] + 1;
                        queue.Enqueue(next);
                    }
                }


                callbacks.Invoke(callbacks.actionIncProgress);
                if (id == 0)
                    cancelToken.ThrowIfCancellationRequested();
                else if (cancelToken.IsCancellationRequested)
                    return;
            }
        }
    }
}
