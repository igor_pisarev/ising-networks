﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ising
{
    public abstract class IsingCalculation
    {
        public Model model;
        public Correlations corr;
        public Form1 callbacks;

        public HashSet<int>[] topology;
        public int[] values;
        public int[,] ts;


        public IsingCalculation(Model model, Form1 callbacks)
        {
            this.model = model;
            this.callbacks = callbacks;
        }

        public abstract void Generate(CancellationToken cancelToken);
        public abstract void GenerateTopology(CancellationToken cancelToken);

        public virtual void BuildCorrelationMatrix(CancellationToken cancelToken)
        {
            corr = new Correlations(model, MathUtils.Sqr(model.n));
        }

        public virtual void BuildCorrelationMatrixParallel(CancellationToken cancelToken)
        {
            corr = new Correlations(model, MathUtils.Sqr(model.n));
        }

        public virtual void CheckTopology(double prob, CancellationToken cancelToken)
        {
            if (callbacks.onlyQueryPath)
            {
                // check topology
                NetworkProcessor proc = new NetworkProcessor(topology, callbacks);
                double l = proc.ProcessAverageShortestLengthPath(cancelToken);
                callbacks.AddShortestPathForExport(prob, l);
            }
        }
    }
}
