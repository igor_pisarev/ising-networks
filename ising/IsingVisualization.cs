﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace ising
{
    public class IsingVisualization : Form
    {
        private int n, d;
        private Graphics gr;

        public void Init(int n)
        {
            this.n = n;
            d = 4;
            Size = new Size(n * d, n * d + SystemInformation.CaptionHeight);

            gr = CreateGraphics();
            gr.Clear(BackColor);
        }

        public void Draw(IsingCalculation calc)
        {
            for (int i = 0; i < calc.values.Length; i++)
                Draw(calc, i);
        }

        public void Draw(int[,] grid)
        {
            for (int i = 0; i < n; i++)
			    for (int j = 0; j < n; j++)
                    Draw(i, j, grid[i, j]);
        }

        public void Draw(int i, int j, int val)
        {
            gr.FillRectangle(new SolidBrush(val == 1 ? Color.Red : Color.Blue), d * j, d * i, d, d);
        }

        public void Draw(IsingCalculation calc, int i)
        {
            int row = i / calc.model.n, col = i % calc.model.n;
            gr.FillRectangle(new SolidBrush(calc.values[i] == 1 ? Color.Red : Color.Blue), d * col, d * row, d, d);
        }
    }
}
